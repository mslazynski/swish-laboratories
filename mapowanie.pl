% tłumaczy element listy
% aby zmienić działanie mapowania
% należy zmienić ciało tej reguły
mapuj(X, Y) :- Y is X * 2.

% przypadek bazowy rekurencji
% zmapowana pusta lista to nadal pusta lista
map([], []).

% reguła mapująca niepustą listę
% tłumaczy pierwszy element, przepisuje go do wyniku
% i następnie rekurencyjnie mapuje ogon
map([Element|Ogon], [PrzetłumaczonyElement|PrzetłumaczonyOgon]) :-
  przetłumacz(Element, PrzetłumaczonyElement),
  mapuj(Ogon, PrzetłumaczonyOgon).