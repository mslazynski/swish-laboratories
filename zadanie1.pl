%%%%%%%%%%%%%%
% Zadanie 1
% Cena: 1 punkt
% Cel: Uzupełnić predykat "gra", by działał zgodnie z opisem
%%%%%%%%%%%%%

%%
% Liczba, którą ma zgadnąć gracz
%%
rozwiazanie(42).

%%
% Ten predykat powinien tak długo pytać gracza o podanie losowej liczby, aż:
% 1) poda liczbę, która jest rozwiązaniem - wtedy wygrywa i wyświetlany
%	 jest napis: "Jesteś jak Tommy Lee Jones w Ściganym!"
% 2) poda tę samą liczbę więcej niż raz - wtedy przegrywa i wyświetlany
%    jest napis: "Powtarzasz się, a to niedobrze"
%%
gra :- fail.

%%%%%
% Podpowiedzi:
% - należy obsłużyć trzy przypadki:
%	1) gracz zgadł, gra się kończy zwycięstwem
%   2) gracz się powtórzył, gra się kończy porażką
%   3) gracz nie zgadł, ale się nie powtórzył, zapamiętujemy
%      podaną liczbę i ponownie wywołujemy predykat "gra",
%      w ten sposób gra się nie skończy
% - należy zastosować assert 
%%%%%