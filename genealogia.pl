%aktualna_data(Aktualny rok)
aktualna_data(data(9, 11, 2015)).

%osoba(Identyfikator, Imię, Nazwisko, Płeć, Urodziny - Śmierć).
osoba(olgierd1, 'Olgierd', 'Jarzyna', m, 
	wydarzenie('Kraków', data(16, 7, 1993)) - brak).
osoba(olgierd2, 'Olgierd', 'Jarzyna', m, 
	wydarzenie('Kraków', data(8, 3, 2012)) - brak).
osoba(marzena, 'Marzena', 'Jarzyna', k, 
	wydarzenie('Kraków', data(11, 3, 1994)) - brak).

%rodzic(Identyfikator Rodzica, Identyfikator Dziecka).
rodzic(olgierd1, olgierd2).
rodzic(marzena, olgierd2).

%małżeństwo(Identyfikator Męża, Identyfikator Żony, Ślub-Rozwód).
małżeństwo(olgierd1, marzena).
