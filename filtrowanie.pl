% tutaj definiujemy warunek, który ma być spełniany
% przez odfiltrowane elementy; należy zmienić ciało
% tej reguły, by zmienić wynik filtrowania
spełnia_warunek(X) :- X > 0.

% przypadek bazowy, filtrowanie pustej listy zwraca
% pustą listę
filtruj([],[]).

% przypadek, gdy element przechodzi przez filtr
% true mówi o tym, że każdy element jest dobry
% ta reguła zawsze się wykona, przepisze
% element na początek wyniku i wykona się 
% rekurencyjne, by przefiltrować kolejne
% elementy
filtruj([Dobry|Ogon], [Dobry|PrzefiltrowanyOgon]) :-
  spełnia_warunek(Dobry),
  filtruj(Ogon, PrzefiltrowanyOgon).

% przypadek, gdy element nie przechodzi przez filtr
% fail mówi o tym, że żaden element nie jest zły
% ta reguła nigdy się nie wykona, pominie zły elment
% i wykona się rekurencyjnie licząc ogon
filtruj([Zły|Ogon], PrzefiltrowanyOgon) :-
  \+ spełnia_warunek(Zły),
  filtruj(Ogon, PrzefiltrowanyOgon).