<div class="notebook">
<div class="nb-cell markdown">
# Przykładowe kolokwium

Poniżej znajduje się baza wiedzy o miastach w Polsce oraz
szereg pytań podobnych do tych, kóre pojawią się na kolokwium
16 listopada. Przykład jest o tyle prostszy, że na prawdziwym kolokwium
nie będzie dostępu do Prologa, by przetestować zapytania, etc.
Każde zadanie na tej stronie ma kilka wariantów, tak aby ułatwić naukę.
</div>

<div class="nb-cell program">

% miasto(Nazwa, Populacja, Powierzchnia).
miasto('Warszawa', 1735442, 51724). miasto('Kraków', 761873, 32685).
miasto('Łódź', 706004, 29325). miasto('Wrocław', 634487, 29282).
miasto('Poznań', 545680, 26191). miasto('Gdańsk', 461489, 26196).

% trasa(Miasto - Miasto, Długość, Szacowany czas przejazdu w minutach).
trasa('Warszawa' - 'Kraków', 294, 244).
trasa('Warszawa' - 'Kraków', 355, 239).
trasa('Warszawa' - 'Kraków', 311, 266).
trasa('Gdańsk' - 'Warszawa', 341, 253).
trasa('Warszawa' - 'Łódź', 130, 92).
trasa('Kraków' - 'Wrocław', 269, 169).

</div>

<div class="nb-cell markdown">
## Zad 1.

Jaką wartość będą miały zmienne X i Y po wykonaniu zapytania:
</div>

<div class="nb-cell query">
miasto(X, _, _), trasa(_ - X, 130, Y).
</div>
<div class="nb-cell query">
trasa(X - Y, _, _), miasto(X, _, 26196), miasto(Y, 1735442, _).
</div>
<div class="nb-cell query">
miasto(X, 706004, _), trasa(Y - X, _, _).
</div>

<div class="nb-cell markdown">
## Zad 2.

Proszę napisać w Prologu zapytanie:

  - 'Ile wynosi populacja miasta Warszawa?'
  - 'Jaka jest powierzchnia Gdańska?'
  - 'Czy istnieje trasa między Warszawą a Krakowem?'
  - 'Czy Gdańsk ma większą populację od Poznania?'
  - 'Jaki jest szacowany czas przejazdu z Warszawy do Wrocławia przez Kraków?'
  - 'Jaka jest gęstość zaludnienia Łodzi?'
 
<div class="nb-cell query">
</div> 

</div>

<div class="nb-cell markdown">
## Zad 3.

Proszę dopisać do bazy predykat:

  - `gęstość/2`, który oblicza gęstość zaludnienia w danym mieście. Pierwszym argumentem ma być określone miasto, drugi natomiast przechowuje wynik. 
  - `prędkość/3`, który oblicza oczekiwaną średnią prędkość przejazdu na danej trasie. Jako pierwszy argument
  przyjmuje długość trasy, jako drugi szacowany czas przejazdu, trzeci argument przechowuje wynik.

W powyższych predykatach proszę nie martwić się jednostkami fizycznymi.
</div>

<div class="nb-cell markdown">
## Zad 4. 

Proszę dopisać do bazy rekurencyjny predykat `dojazd/2`,który wskazuje, czy 
istnieje pośrednia lub bezpośrednia trasa między dwoma miastami. Predykat 
powinien abstrahować od kierunku trasy (`X - Y` i `Y - X` są dla niego równoważne).

</div>

<div class="nb-cell markdown">
## Zad 5.

Proszę napisać zapytanie, które:

  - policzy średnią długość trasy z Warszawy do Krakowa
  - policzy średni rozmiar populacji w miastach, do których istnieje trasa z Warszawy
  - znajdzie w bazie miasto o największej populacji.

W razie potrzeby, można (należy) dopisać brakujące predykaty do bazy wiedzy. 
Dla przypomnienia, predykat `length/2` oblicza długość listy. 

</div>

<div class="nb-cell query">
</div>

<div class="nb-cell markdown">
## Zad 6 

Zadanie dla ambitnych za dodatkowy punkt. 

  1. Proszę zmodyfikować predykat `dojazd/2` z Zadania 4 tak, aby oprócz sprawdzenia, czy istnieje dojazd, podawał również jego długość i szacowany czas przejazdu. Nowy predykat powinien nazywać się `dojazd/4`.
  2. Proszę zmodyfikować predykat `dojazd/2` z Zadania 4 tak, aby oprócz sprawdzenia, czy istnieje dojazd, podawał również listę miast, przez które trzeba przejechać. Miasta przejazdowe powinny być zwrócone jako lista w 3 argumencie - nowy predykat nosi zatem miano `dojazd/3`.
</div>
</div>

