% Dynamiczny predykat służący
% przechowywaniu imion znajomych
:- dynamic jest_znajomym/1.

% Predykat 0-argumentowy, który wywołuję procedurę
% dodawania nowych znajomych.
%
% Oczekiwane działanie:
% 1. Powinien zapytań o imię nowego znajomego 
% 2. Wywołać predykat dodaj_znajomego/1 z wczytanym imieniem
%    jako argumentem
dodaj_znajomego :- 
	fail.

% Predykat 1-argumentowy, który sprawdza, czy znajomy
% o imieniu podanym w argumencie jest już w bazie.
%
% Predykat zdefiniowany jest przez dwie reguły.
% Pierwsza dotyczy przypadku, gdy znajomy jest już w bazie
% (trzeba to sprawdzić w pierwszym warunku reguły);
% powinna ona wyświetić komunikat "Już go znam!".
dodaj_znajomego(X) :-
	fail.

% Druga reguła dotyczy przypadku, gdy znajomego nie ma w bazie
% (trzeba to sprawdzić w pierwszym warunku ciała reguły);
% powinna dodać znajomego do bazy używać predykatu jest_znajomym/1
% i wyświetić komunikat "Nowy znajomy zapamiętany"
dodaj_znajomego(X) :-
	fail.


