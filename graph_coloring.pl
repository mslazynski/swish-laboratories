% wierzchołek
v(a). v(b). v(c). v(d). v(e).

% krawędź skierowana ed(id początku, id końca)
ed(a, b).
ed(a, c).
ed(a, d).
ed(a, e).
ed(b, c).
ed(c, d).
ed(d, e).

% krawędź nieskierowana
e(V1,V2) :- ed(V1,V2).
e(V2,V1) :- \+ed(V1,V2), ed(V2,V1).

%%%%%%%%%%%%%
%%%%%%%%% Część właściwa
%%%%%%%%%%%%%

% dziedziny zmiennych, czyli dostępne kolory
color(red).
color(green).
color(yellow).
%color(brown).


solution(Graf) :-
  zmienne(Zmienne),
  dziedziny(Zmienne),
  ograniczenia(Zmienne),
  rysuj(Zmienne, Graf).
 
zmienne(Z) :-
  findall(zmienna(V,_), v(V), Z).

dziedziny(Z) :- maplist(przypisz_kolor, Z).
przypisz_kolor(zmienna(_,C)) :- color(C).

diff(kolory(X,Y)) :- X \= Y.
ograniczenia(Z) :- findall(kolory(C1,C2), 
    (e(V1,V2), member(zmienna(V1,C1), Z), member(zmienna(V2,C2), Z)), C),
    maplist(diff, C).

%%%%%%%%%%%%%%%%%%%%% 
% rysowanie
%%%
:- use_rendering(graphviz).
rysuj(Zmienne, Graf) :- nodes(Zmienne, Ns), edges(Es),
               append([Ns,Es], Elements),
               Graf = graph(Elements).

zmienna2node(zmienna(V,C), node(V, [style=filled, fillcolor=C])).
nodes(Zmienne, Ns) :-maplist(zmienna2node, Zmienne, Ns).

edges(Es) :-
  findall(V1 - V2, e(V1,V2), Es).